#
# Module: essioc
#
require essioc

#
# Module: vac_ctrl_mks946_937b
#
require vac_ctrl_mks946_937b,4.5.2


#
# Module: essioc
#
iocshLoad("${essioc_DIR}/common_config.iocsh")

#
# Device: Spk-050Crm:VAC-VEG-01100
# Module: vac_ctrl_mks946_937b
# Todo: Check if the port is  as well as serial numbers for SLOT Board
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_ctrl_mks946_937b_moxa.iocsh", "DEVICENAME = Spk-050Crm:VAC-VEG-01100, BOARD_A_SERIAL_NUMBER = 1807120950, BOARD_B_SERIAL_NUMBER = 1609081505, BOARD_C_SERIAL_NUMBER = 1609080803, IPADDR = moxa-vac-spk-30-u006.tn.esss.lu.se, PORT = 4001")

#
# Device: Spk-050Crm:Vac-VGP-01100
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgp.iocsh", "DEVICENAME = Spk-050Crm:Vac-VGP-01100, CHANNEL = A1, CONTROLLERNAME = Spk-050Crm:VAC-VEG-01100")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-050Crm:Vac-VGP-01100, RELAY = 1, RELAY_DESC = 'Process PLC: '")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-050Crm:Vac-VGP-01100, RELAY = 2, RELAY_DESC = 'Process PLC: Not used'")

#
# Device: Spk-050Crm:Vac-VGC-01100
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgc.iocsh", "DEVICENAME = Spk-050Crm:Vac-VGC-01100, CHANNEL = B1, CONTROLLERNAME = Spk-050Crm:VAC-VEG-01100")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-050Crm:Vac-VGC-01100, RELAY = 1, RELAY_DESC = 'not wired'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-050Crm:Vac-VGC-01100, RELAY = 2, RELAY_DESC = 'Process PLC:'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-050Crm:Vac-VGC-01100, RELAY = 3, RELAY_DESC = 'Process PLC: Not used'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-050Crm:Vac-VGC-01100, RELAY = 4, RELAY_DESC = 'not wired'")

#
# Device: Spk-060Crm:Vac-VGP-01100
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgp.iocsh", "DEVICENAME = Spk-060Crm:Vac-VGP-01100, CHANNEL = A2, CONTROLLERNAME = Spk-050Crm:VAC-VEG-01100")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-060Crm:Vac-VGP-01100, RELAY = 1, RELAY_DESC = 'Process PLC: '")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-060Crm:Vac-VGP-01100, RELAY = 2, RELAY_DESC = 'Process PLC: Not used'")

#
# Device: Spk-060Crm:Vac-VGC-01100
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgc.iocsh", "DEVICENAME = Spk-060Crm:Vac-VGC-01100, CHANNEL = C1, CONTROLLERNAME = Spk-050Crm:VAC-VEG-01100")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-060Crm:Vac-VGC-01100, RELAY = 1, RELAY_DESC = 'not wired'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-060Crm:Vac-VGC-01100, RELAY = 2, RELAY_DESC = 'Process PLC:'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-060Crm:Vac-VGC-01100, RELAY = 3, RELAY_DESC = 'Process PLC: Not used'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-060Crm:Vac-VGC-01100, RELAY = 4, RELAY_DESC = 'not wired'")