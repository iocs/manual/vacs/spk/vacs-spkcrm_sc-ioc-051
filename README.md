# VacS-SpkCrm_SC-IOC-051

IOC for Spk-050Crm MKS Gauge Controller

---

## Used Module

IOC uses [e3-vac_ctrl_mks946_937b](https://gitlab.esss.lu.se/e3/wrappers/vac/e3-vac_ctrl_mks946_937b)

## Devices

**Controller:** 
- Spk-050Crm:VAC-VEG-01100

**Controlled Devices:**
- Spk-050Crm:Vac-VGP-01100
- Spk-050Crm:Vac-VGC-01100
- Spk-060Crm:Vac-VGP-01100
- Spk-060Crm:Vac-VGC-01100

